// function square(x) {
//     return x * x;
// };

// console.log(square(3));

// // const squareArrow = (x) => {
// //     return x * x;
// // };

// const squareArrow = (x) => x * x;

// console.log(squareArrow(7));

// Challenge
const getFirstNameArrow = (fullName) => {
  return fullName.split(' ')[0];
};

const getFirstNameArrowExpression = (fullName) => fullName.split(' ')[0];

console.log(getFirstNameArrow("Ozone Kafley"));
console.log(getFirstNameArrowExpression("Mike McCall"));