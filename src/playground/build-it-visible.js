class VisibilityToggle extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
    this.state = {
      visibility: false
    }
  }
  handleToggleVisibility() {
    this.setState((prevState) => {
      return {
        visibility: !prevState.visibility
      };
    });
  }
  render() {
    return (
      <div>
        <h1>Visibility Toggle</h1>
        <button onClick={this.handleToggleVisibility}>
          {this.state.visibility ? 'Hide details' : 'Show details'}
        </button>
        {this.state.visibility && <p>Here are the hidden details!</p>}
      </div>
    );
  }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));

// const appRoot = document.getElementById('app');

// const app = {
//   detailsShown: false
// };

// const onVisibilityToggle = () => {
//   app.detailsShown = !app.detailsShown
//   render();
// };

// const render = () => {
//   const template = (
//     <div>
//       <h1>Visibility Toggle</h1>
//       <button onClick={onVisibilityToggle}>
//         {app.detailsShown ? 'Hide details' : 'Show details'}
//       </button>
//       {app.detailsShown && <p>Here are the details!</p>}
//     </div>
//   );

//   ReactDOM.render(template, appRoot);
// };

// render();


