// arguments object - no longer bound with arrow functions

const add = (a, b) => {
    // console.log(arguments)
    return a + b;
};
console.log(add(33, 2, 2343, 23423));

// this keyword - no longer bound

const user = {
    name: 'Ozone',
    cities: ['Jacksonville', 'Fort Worth', 'Austin'],
    printPlacesLived() {
        return this.cities.map((city) => city);
    }
};
console.log(user.printPlacesLived());

// Challenge area:

const multiplier = {
    numbers: [2, 4, 5],
    multiplyBy: 3,
    multiply() {
        return this.numbers.map((number) => number * this.multiplyBy);
    }
};

console.log(multiplier.multiply());